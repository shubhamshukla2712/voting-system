package com.example.votingsystem;

import java.lang.reflect.Array;
import java.util.Random;


public class RandomKey
{
    private Random rand;
    private static Exception e = null;
    public  String key = "";
    private static int inputSize = 0,tempSizeLower = 0,tempSizeUpper = 0,tempSizeSpecial = 0,tempSizeNumbers = 0, specialTime = 0, size = 15;
    private static String[] upperCaseAlphabets = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    private static String[] lowerCaseAlphabets = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    private static int[] numbers ={0,1,2,3,4,5,6,7,8,9};
    //private static char[] specialCharacter = {'`','~','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[',']','}','|','/',':','"',':',';','<',',','>','.','?',','};

    public String createRandomKey()
    {
        rand = new Random();

        specialTime = size % 3;

        for(int i = 0; i < size/3 ; i++)
        {
            tempSizeLower = rand.nextInt(lowerCaseAlphabets.length);
            tempSizeUpper = rand.nextInt(upperCaseAlphabets.length);
          //  tempSizeSpecial = rand.nextInt(specialCharacter.length);
            tempSizeNumbers = rand.nextInt(numbers.length);

            key = key.concat(lowerCaseAlphabets[tempSizeLower]);
            key = key.concat(upperCaseAlphabets[tempSizeUpper]);
           // key = key.concat(Character.toString(Array.getChar(specialCharacter,tempSizeSpecial)));
            key = key.concat(Integer.toString(Array.getInt(numbers,tempSizeNumbers)));
        }

        if(specialTime > 0  && specialTime < 3)
        {
            switch (specialTime)
            {
                case 1:
                    tempSizeLower = rand.nextInt(lowerCaseAlphabets.length);
                    key = key.concat(lowerCaseAlphabets[tempSizeLower]);
                    break;

                case 2:
                    tempSizeLower = rand.nextInt(lowerCaseAlphabets.length);
                    tempSizeUpper = rand.nextInt(upperCaseAlphabets.length);
                    key = key.concat(lowerCaseAlphabets[tempSizeLower]);
                    key = key.concat(upperCaseAlphabets[tempSizeUpper]);
                    break;

//                case 3:
//                    tempSizeLower = rand.nextInt(lowerCaseAlphabets.length);
//                    tempSizeUpper = rand.nextInt(upperCaseAlphabets.length);
//                    tempSizeSpecial = rand.nextInt(specialCharacter.length);
//                    key = key.concat(lowerCaseAlphabets[tempSizeLower]);
//                    key = key.concat(upperCaseAlphabets[tempSizeUpper]);
//                    key = key.concat(Character.toString(Array.getChar(specialCharacter,tempSizeSpecial)));
//                    break;

                default:
                    return key;
            }
        }
        return key;
    }
}


