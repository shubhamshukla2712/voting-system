package com.example.votingsystem;

public class RegisterModel
{
    private String uid;
    private String name;
    private String email;
    private String contact;
    private String password;
    private boolean isVotingDone;
    private String partyToVote;

    public RegisterModel() {
    }

    public RegisterModel(String uid, String name, String email, String contact, String password) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.password = password;
    }

    public RegisterModel(boolean isVotingDone,String partyToVote, String uid) {
        this.isVotingDone = isVotingDone;
        this.partyToVote = partyToVote;
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVotingDone() {
        return isVotingDone;
    }

    public void setVotingDone(boolean votingDone) {
        isVotingDone = votingDone;
    }

    public String getPartyToVote() {
        return partyToVote;
    }

    public void setPartyToVote(String partyToVote) {
        this.partyToVote = partyToVote;
    }
}
