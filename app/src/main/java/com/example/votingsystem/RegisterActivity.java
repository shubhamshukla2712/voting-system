package com.example.votingsystem;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity
{

    private EditText emailEdtR, passwordEdtR, nameEdtR, confirmPasswordEdtR, contactEdtR;
    private Button registerUserBtnR;
    private String email, password, name, confirmPassword, contact;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);

        emailEdtR = findViewById(R.id.emailEdtR);
        passwordEdtR = findViewById(R.id.passwordEdtR);
        nameEdtR = findViewById(R.id.nameEdtR);
        confirmPasswordEdtR = findViewById(R.id.confirmPasswordEdtR);
        contactEdtR = findViewById(R.id.contactEdtR);
        registerUserBtnR = findViewById(R.id.registerUserBtnR);

        registerUserBtnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateEntry();
            }
        });
    }

    private void validateEntry()
    {
        email = emailEdtR.getText().toString();
        name = nameEdtR.getText().toString();
        password = passwordEdtR.getText().toString();
        confirmPassword = confirmPasswordEdtR.getText().toString();
        contact = contactEdtR.getText().toString();

        if(TextUtils.isEmpty(email))
        {
            progressDialog.dismiss();
            Toast.makeText(RegisterActivity.this,"Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            if(!validateEmailAddress(email))
            {
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this,"Invalid email address", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                if(TextUtils.isEmpty(name))
                {
                    progressDialog.dismiss();
                    Toast.makeText(RegisterActivity.this,"Please enter name", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                {
                    if(TextUtils.isEmpty(contact))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this,"Please enter contact", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else
                    {
                        if(!isValidMobile(contact))
                        {
                            progressDialog.dismiss();
                            Toast.makeText(RegisterActivity.this,"Invalid Number", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        else
                        {
                            if(TextUtils.isEmpty(password))
                            {
                                progressDialog.dismiss();
                                Toast.makeText(RegisterActivity.this,"Please enter password", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else
                            {
                                if(password.length() < 6)
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(RegisterActivity.this,"Password should be greater than 5 charcter  ", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                else
                                {
                                    if(TextUtils.isEmpty(confirmPassword))
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(RegisterActivity.this,"Please enter confirm Password", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    else
                                    {
                                        if(!password.equals(confirmPassword))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(RegisterActivity.this,"Password enter same password", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        else
                                        {
                                            createAccount();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void createAccount()
    {
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if(task.isSuccessful())
                {
                    String uid = firebaseAuth.getUid();
                    RegisterModel registerModel = new RegisterModel(uid, name,email, contact, password);
                    databaseReference.child("user_details").child(uid).setValue(registerModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if(task.isSuccessful())
                            {
                                progressDialog.hide();
                                Toast.makeText(RegisterActivity.this,"Account Created Sucessfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

    private boolean validateEmailAddress(String emailAddress){
        String  expression="^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }
}
