package com.example.votingsystem;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PartyListAdapter extends RecyclerView.Adapter<PartyListAdapter.ViewHolder>
{
    private Context context;
    private static List<VoteModel> voteModelList;
    private ButtonAdapterListener listener;
    public static DatabaseReference databaseReference;
    public static int vote = 0;
    public static FirebaseAuth firebaseAuth;

    public PartyListAdapter(Context context,List<VoteModel> voteModelList)
    {
        this.context = context;
        this.voteModelList = voteModelList;
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_partylist, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view,listener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        VoteModel voteModel = voteModelList.get(i);
        viewHolder.partyNameU.setText(voteModel.getName());

        Glide.with(context).load(voteModel.getImage()).placeholder(R.drawable.ic_launcher_background).into(viewHolder.partyIconU);
        getItemId(i);
    }

    @Override
    public int getItemCount() {
        return voteModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView partyIconU;
        public TextView partyNameU;
        public Button voteBtn;
        private Context context;

        public ViewHolder(@NonNull View itemView,ButtonAdapterListener listener)
        {
            super(itemView);
            partyIconU = itemView.findViewById(R.id.partyIconU);
            partyNameU = itemView.findViewById(R.id.partyNameU);
            voteBtn = itemView.findViewById(R.id.voteBtn);
            context = itemView.getContext();
            voteBtn.setOnClickListener(this);

            //// CHANGING BUTTON COLOR ////////

            databaseReference.child("voting_status").child(firebaseAuth.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.hasChildren())
                    {
                        RegisterModel registerModel = dataSnapshot.getValue(RegisterModel.class);
                        if(registerModel.isVotingDone())
                        {
                            voteBtn.setText("Already Voted");
                            changeButtonColor(registerModel.getPartyToVote());
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            ////
        }

        @Override
        public void onClick(View v)
        {
            VoteModel voteModel = voteModelList.get(getAdapterPosition());
            if (v.getId() == voteBtn.getId())
            {
               if(voteBtn.getText().equals("Already Voted"))
               {
                   Toast.makeText(context,"You cannot vote twice",Toast.LENGTH_SHORT).show();
               }
               else
               {
                   final String partyKey = voteModel.getPartykey();
                   final String partyName = voteModel.getName();
                   String voteCount = voteModel.getCount();
                   String newCount;
                   vote = Integer.parseInt(voteCount);
                   vote = vote + 1;

                   newCount = String.valueOf(vote++);

                   Map<String,Object> map = new HashMap<>();
                   map.put("count",newCount);
                   databaseReference.child("party_details").child(partyKey).updateChildren(map);

                   RegisterModel registerModelUpdate = new RegisterModel(true,partyName, firebaseAuth.getUid());
                   databaseReference.child("voting_status").child(firebaseAuth.getUid()).setValue(registerModelUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task)
                       {
                          if(task.isSuccessful())
                          {
                              Toast.makeText(context,"You have voted for "+partyName, Toast.LENGTH_SHORT).show();
                              changeButtonColor(partyName);
                          }
                       }
                   });

               }
            }
        }

        private void changeButtonColor(String name)
        {
            try
            {
                VoteModel voteModel = voteModelList.get(getAdapterPosition());
                if(voteModel.getName() != null && !TextUtils.isEmpty(voteModel.getName()))
                {
                    if(voteModel.getName().equals(name))
                    {
                        Resources resources = context.getResources();
                        Drawable drawable = resources.getDrawable(R.color.green);
                        voteBtn.setBackgroundDrawable(drawable);
                        voteBtn.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                    else
                    {
                        Resources resources = context.getResources();
                        Drawable drawable = resources.getDrawable(R.color.red);
                        voteBtn.setBackgroundDrawable(drawable);
                        voteBtn.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public interface ButtonAdapterListener
    {
        void onPositionClicked(int position);
    }
}
