package com.example.votingsystem;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    private RecyclerView recyclerView;
    private String loginId;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private String userName = null;
    private boolean isAdminLogin = false;
    private List<VoteModel> list;
    private FloatingActionButton fab;
    private NavigationView navigationView;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Start Voting");
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(HomeScreen.this,AddPartyActivity.class));
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
         navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView = findViewById(R.id.recyclerviewPartylist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(HomeScreen.this));
        progressDialog = new ProgressDialog(this);
        list = new ArrayList<>();

        loginId = firebaseAuth.getUid();
        loadUserData();
        //loadPartyList();

    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onStart() {
        super.onStart();
        if(!firebaseAuth.getUid().equals("LjSI7enEUTWGzuTaiMrZ9kPWfho2"))
        {
            fab.setVisibility(View.GONE);
            fab.setEnabled(false);
        }
        else
        {
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_profile);
            target.setVisible(false);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(isAdminLogin)
        {
            if (id == R.id.nav_profile)
            {

            }
            else if (id == R.id.nav_logout)
            {
                logoutUser();
            }
        }
        else
        {
            if (id == R.id.nav_profile)
            {
                startActivity(new Intent(HomeScreen.this, ProfilePage.class));
            }
            else if (id == R.id.nav_logout)
            {
                logoutUser();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutUser()
    {
       firebaseAuth.signOut();
       finish();
       startActivity(new Intent(HomeScreen.this,MainActivity.class));
    }

    private void loadUserData()
    {
        progressDialog.setMessage("Loading user Data");
        progressDialog.show();

        databaseReference.child("user_details").child(firebaseAuth.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChildren())
                {
                    RegisterModel registerModel = dataSnapshot.getValue(RegisterModel.class);
                    userName = registerModel.getUid();
                    if(userName.equals("LjSI7enEUTWGzuTaiMrZ9kPWfho2"))
                    {
                        isAdminLogin = true;
                    }
                    progressDialog.hide();
                    loadPartyList();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    private void loadPartyList()
    {
        if(isAdminLogin)
        {
            databaseReference.child("party_details").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    list.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        VoteModel voteModel = snapshot.getValue(VoteModel.class);
                        list.add(voteModel);
                    }

                    AdminRecyclerviewAdapter adapter = new AdminRecyclerviewAdapter(HomeScreen.this, list);
                    recyclerView.setAdapter(adapter);
                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        else
        {
            databaseReference.child("party_details").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    list.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        VoteModel voteModel = snapshot.getValue(VoteModel.class);
                        list.add(voteModel);
                    }

                    PartyListAdapter adapter = new PartyListAdapter(HomeScreen.this, list);
                    recyclerView.setAdapter(adapter);
                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
}
