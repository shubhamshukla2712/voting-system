package com.example.votingsystem;

public class VoteModel
{
    private String name;
    private String partykey;
    private String image;
    private String count;

    public VoteModel() {
    }

    public VoteModel(String name, String image) {
        this.name = name;
    }

    public VoteModel(String name, String partykey, String count) {
        this.name = name;
        this.partykey = partykey;
        this.count = count;
    }

    public VoteModel(String count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPartykey() {
        return partykey;
    }

    public void setPartykey(String partykey) {
        this.partykey = partykey;
    }
}
