package com.example.votingsystem;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ProfilePage extends AppCompatActivity
{

    private TextView PersonEmailTxt, PersonContactTxt, PersonNameTxt, PersonPasswordTxt;
    private Button editNameBtn, editContactBtn;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        PersonEmailTxt = findViewById(R.id.PersonEmailTxt);
        PersonContactTxt = findViewById(R.id.PersonContactTxt);
        PersonNameTxt = findViewById(R.id.PersonNameTxt);
        PersonPasswordTxt = findViewById(R.id.PersonPasswordTxt);
        editNameBtn = findViewById(R.id.editNameBtn);
        editContactBtn = findViewById(R.id.editContactBtn);
        progressDialog = new ProgressDialog(this);

        loadUserData();

        editNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                editNameFunc();
            }
        });

        editContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                editContactFunc();
            }
        });
    }

    private void loadUserData()
    {
        progressDialog.setMessage("Loading user Data");
        progressDialog.show();

        databaseReference.child("user_details").child(firebaseAuth.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChildren())
                {
                    RegisterModel registerModel = dataSnapshot.getValue(RegisterModel.class);
                    PersonEmailTxt.setText(registerModel.getEmail());
                    PersonContactTxt.setText(registerModel.getContact());
                    PersonNameTxt.setText(registerModel.getName());
                    PersonPasswordTxt.setText(registerModel.getPassword());
                    progressDialog.hide();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    private void editNameFunc()
    {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_name_custom_popup);

        // set the custom dialog components - text and button
        final EditText editNewName =  dialog.findViewById(R.id.editNewName);
        final Button submit = dialog.findViewById(R.id.changeNameBtn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String newName = editNewName.getText().toString();
                if(TextUtils.isEmpty(newName))
                {
                    Toast.makeText(ProfilePage.this,"Invalid Name",Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                {
                    Map<String,Object> map = new HashMap<>();
                    map.put("name",newName);
                    databaseReference.child("user_details").child(firebaseAuth.getUid()).updateChildren(map);
                    dialog.hide();
                }
            }
        });

        dialog.show();
    }

    private void editContactFunc()
    {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_contact_custom_popup);

        // set the custom dialog components - text and button
        final EditText editNewNumber =  dialog.findViewById(R.id.editNewNumber);
        final Button submit = dialog.findViewById(R.id.changeNumberBtn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String newNumber = editNewNumber.getText().toString().trim();
                if(TextUtils.isEmpty(newNumber))
                {
                    Toast.makeText(ProfilePage.this,"Please enter number",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(!isValidMobile(newNumber))
                    {
                        Toast.makeText(ProfilePage.this,"Invalid Number",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Map<String,Object> map = new HashMap<>();
                        map.put("contact",newNumber);
                        databaseReference.child("user_details").child(firebaseAuth.getUid()).updateChildren(map);
                        dialog.hide();
                    }
                }
            }
        });
        dialog.show();
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
