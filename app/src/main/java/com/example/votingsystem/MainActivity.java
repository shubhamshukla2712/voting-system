package com.example.votingsystem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity
{
    private EditText emailEdt, passwordEdt;
    private Button loginBtn, registerBtn;
    private String email, password;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);


        emailEdt = findViewById(R.id.emailEdt);
        passwordEdt = findViewById(R.id.passwordEdt);

        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               validateEntry();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(MainActivity.this,RegisterActivity.class));
            }
        });
        checkIfUserIsSignIn();
    }

    private void validateEntry()
    {
        email = emailEdt.getText().toString().trim();
        password = passwordEdt.getText().toString();

        if(TextUtils.isEmpty(email))
        {
            progressDialog.dismiss();
            Toast.makeText(MainActivity.this,"Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            if(!validateEmailAddress(email))
            {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this,"Invalid email address", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                if(TextUtils.isEmpty(password))
                {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this,"Please enter password", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                {
                    if(password.length() < 6)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this,"Password should be greater than 5 charcter  ", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else
                    {
                        signInUser();
                    }
                }
            }
        }
    }

    private void signInUser()
    {
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if(task.isSuccessful())
                {
                    progressDialog.dismiss();
                    //Take to loginpage
                    finish();
                    startActivity(new Intent(MainActivity.this,HomeScreen.class));
                }
            }
        });
    }

    private void checkIfUserIsSignIn()
    {
        try
        {
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
            if(currentUser == null)
            {
                return;
            }
            else
            {
                finish();
                startActivity(new Intent(MainActivity.this, HomeScreen.class));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private boolean validateEmailAddress(String emailAddress){
        String  expression="^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

}
