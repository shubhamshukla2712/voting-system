package com.example.votingsystem;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class AdminRecyclerviewAdapter extends RecyclerView.Adapter<AdminRecyclerviewAdapter.ViewHolder>
{
    private Context context;
    private static List<VoteModel> voteModelList;

    public AdminRecyclerviewAdapter(Context context, List<VoteModel> voteModelList)
    {
        this.context = context;
        this.voteModelList = voteModelList;
    }

    @NonNull
    @Override
    public AdminRecyclerviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_party_result, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminRecyclerviewAdapter.ViewHolder viewHolder, int i)
    {
        VoteModel voteModel = voteModelList.get(i);
        viewHolder.partyNameA.setText(voteModel.getName());
        viewHolder.voteCount.setText("Count : "+voteModel.getCount());

        Glide.with(context).load(voteModel.getImage()).placeholder(R.drawable.ic_launcher_background).into(viewHolder.partyIconA);

        getItemId(i);
    }

    @Override
    public int getItemCount() {
        return voteModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView partyIconA;
        public TextView partyNameA,voteCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            partyIconA = itemView.findViewById(R.id.partyIconA);
            partyNameA = itemView.findViewById(R.id.partyNameA);
            voteCount = itemView.findViewById(R.id.voteCount);
        }
    }

}
