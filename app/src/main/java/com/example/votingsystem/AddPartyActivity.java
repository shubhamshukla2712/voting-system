package com.example.votingsystem;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

public class AddPartyActivity extends AppCompatActivity
{

    private EditText partyNameEdt;
    private ImageView partyIcon;
    private Button addpartyBtn;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;
    private AlertDialog.Builder alertDialog;
    private static final int SELECT_IMAGE_FROM_GALLERY = 2;
    private Uri uri;
    private ProgressDialog progressDialog;
    private String partyKey;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_party);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        firebaseAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);

        partyNameEdt = findViewById(R.id.partyNameEdt);
        partyIcon = findViewById(R.id.partyIcon);
        addpartyBtn = findViewById(R.id.addpartyBtn);


        RandomKey randomKey = new RandomKey();
        partyKey = randomKey.createRandomKey();

        partyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectingProfilePic();
            }
        });

        addpartyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                addParty();
            }
        });

    }

    private void selectingProfilePic() {
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("SET PROFILE PIC ").setMessage("Choose Action");

        // add the buttons

        alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intentGallery = new Intent();
                intentGallery.setType("image/*");
                intentGallery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intentGallery, "Select Picture"), SELECT_IMAGE_FROM_GALLERY);
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    private void imageUpload()
    {
        if(uri != null)
        {
            storageReference  = storageReference.child("party_images").child(partyKey);
            storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                {
                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri)
                        {
                            Uri downloadUrl = uri;
                            Map<String,Object> map = new HashMap<>();
                            map.put("image",downloadUrl.toString());
                            map.put("partykey",partyKey);

                            databaseReference.child("party_details").child(partyKey).updateChildren(map);
                        }
                    });
                    progressDialog.dismiss();
                    Toast.makeText(AddPartyActivity.this,"New Party Added",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(AddPartyActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }

        else
        {
            Toast.makeText(AddPartyActivity.this,"No Image is Selected",Toast.LENGTH_SHORT).show();
        }
    }

    private void addParty()
    {
        String partyName = partyNameEdt.getText().toString();
        if(TextUtils.isEmpty(partyName))
        {
            Toast.makeText(AddPartyActivity.this,"Please Enter party name",Toast.LENGTH_SHORT).show();
            return;
        }

        else
        {
            progressDialog.setMessage("Adding new party...Please wait");
            progressDialog.show();
            VoteModel voteModel = new VoteModel(partyName,partyKey,"0");
            databaseReference.child("party_details").child(partyKey).setValue(voteModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task)
                {
                   if(task.isSuccessful())
                   {
                      imageUpload();
                   }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Displaying Image from Gallery
        if (requestCode == SELECT_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            uri = data.getData();
            partyIcon.setImageURI(uri);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
